# Traitement 

## Description 🔍

Ce projet a pour objectif de traiter les données brutes des patients hospitalisés issues de fichiers de données d'hospitalisation, de l'extraction à la préparation de fichiers agrégés. Le projet est organisé avec plusieurs scripts Python incorporés dans un DAG Airflow pour simuler un processus d'alimentation régulière et la gestion du monitoring à chaque étape. 

## Fonctionnalités 🛠️

-----

⚠️ Prérequis :

- **Avant d'installer et de lancer l'application, assurez-vous d'avoir les éléments suivants :**

- Python 3.7 ou supérieur
- Apache Airflow
- Base de donnée relationnelle SGBD (type Postgres) pour l'élaboration du Data Warehouse
- pip - le gestionnaire de paquets Python
	
## Structure du Projet

```./

	├── courses/                # Contenu pédagogique du module de cours
	├── scripts/            # Répertoire pour les données extraites
	│   	└── custom_csv_logger.py  # Fichier de config de la construction des CSV
	│	└── dag_amiens.py         # Fichier de dags à incorporer dans Airflow
	│	└── extract/
	│			└── initial_extraction.py  # Vérification des dossiers et de l'ordre chronologique dans lesquels se trouve les données 
	│			└── upload_data.py  	# Upload des données au sein du datalake du projet
	│	└── load/
	│			└── aggregation.py 	# Aggrégation au format souhaité (temps d'hospit/sexe/nombre de médicament par séjour) par mois d'extraction
	│			└── concatenation.py	# Construction du fichier d'aggrégation global et gestion de l'archivage de ces fichiers
	│	└── logging		# Fichier .conf de configuration des logs
	│	└── transform/        # Layout principal avec informations générale
	│			└── dictionnary.py # Viens récupérer le dictionnaire des données présents dans le les *médicaments, afin d'établir (plus tard) une liste des traitements/surveillances biométriques/biologiques dans le décompte des médicaments par séjour
	│			└── distinct_management.py # Fichier pour la dataquality dans les différents exports, exemple ici dans les doublons
	├── data/                  # Fichiers de données initiaux 
	├── outcomes/              # Fichier d'application Dash
	│   	└── archive/		# Fichier de données aggrégés archivés
	│	└── latest_full_export.csv # Fichier de concaténation des données agrégées
	│	└── dataquality/
	│			└──  patient_data_double.csv # Exemple de fichier de dataquality avec visionnage des données en double entre les exports
	│	└── datalake/
	│			└── .csv # Datalake des données brutes, par type
	│	└── logs/        # Layout principal avec informations générale
	│			└── task_logs.log	  # Fichier logs issus des XComs d'événements en .csv et .log
	│	└── clean_monthly_export/        # Fichier d'exportation mensuel des données aggrégées
	└── README.md              # Fichier de documentation
```

## Flux Airflow : 

![](../png/01.png)

Flux de script linéaire avec fonction de vérification directement au sein des scripts.
Fonction de pull and push XCom afin de récupérer les logs programmés des scripts directement au sein de fichiers CSV avec timestamp/script et autres options.

## Documentation méthodologique du processus d'ETL des données 

![](../png/02.png)

Toutes les fonctions ne sont pas encore implémentées.

## Built With 💻

<img src="https://user-images.githubusercontent.com/25181517/183423507-c056a6f9-1ba8-4312-a350-19bcbc5a8697.png" align="center" height="40" width="40"/> <img src="https://user-images.githubusercontent.com/25181517/117207330-263ba280-adf4-11eb-9b97-0ac5b40bc3be.png" align="center" height="40" width="40"/> <img src="https://upload.wikimedia.org/wikipedia/commons/d/de/AirflowLogo.png" align="center" height="40" width="40"/>



