"""
# Concaténation des fichiers CSV
# Path: dags/rea_amiens/scripts/concatenate_csv.py
"""

# Importation des libraries
import pandas as pd
import os
import logging
import shutil
import sys
from datetime import datetime

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from extract import data_direction, datalake, outcomes
from custom_csv_logger import CSVLoggerHandler, CSVFormatter

# Charger la configuration du logging
logging.config.fileConfig('/opt/airflow/dags/rea_amiens/logging.conf')

# Créer un logger personnalisé
logger = logging.getLogger('custom')

def load_csv(file_path):
    """
    Charge le fichier CSV et retourne un DataFrame pandas.
    """
    try:
        df = pd.read_csv(file_path, sep=';')
        return df
    except FileNotFoundError:
        logger.error(f"File not found: {file_path}")
        return None
    except Exception as e:
        logger.error(f"Error loading CSV: {e}")
        return None

def archive_existing_file(file_path, archive_dir):
    """
    Déplace le fichier existant vers un dossier d'archivage.
    
    Arguments:
    file_path -- Chemin du fichier existant
    archive_dir -- Chemin du répertoire d'archivage
    """
    try:
        if not os.path.exists(archive_dir):
            os.makedirs(archive_dir)
        
        timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
        archive_file_path = os.path.join(archive_dir, f"concatenated_data_{timestamp}.csv")
        shutil.move(file_path, archive_file_path)
        logger.info(f"Fichier existant archivé sous : {archive_file_path}")
    except Exception as e:
        logger.error(f"Erreur lors de l'archivage du fichier existant: {e}")

def concatenate_csv_files(directory, output_file):
    """
    Concatène tous les fichiers CSV d'un répertoire en un seul fichier CSV.
    
    Arguments:
    directory -- Chemin du répertoire contenant les fichiers CSV
    output_file -- Chemin du fichier de sortie
    """
    try:
        all_data = []
        for file_name in os.listdir(directory):
            if file_name.endswith('.csv'):
                file_path = os.path.join(directory, file_name)
                df = load_csv(file_path)
                if df is not None:
                    all_data.append(df)
        
        if all_data:
            concatenated_df = pd.concat(all_data, ignore_index=True)
            concatenated_df.to_csv(output_file, index=False, sep=';')
            logger.info(f"Fichiers concaténés sauvegardés sous : {output_file}")
        else:
            logger.warning("Aucun fichier à concaténer.")
    except Exception as e:
        logger.error(f"Erreur lors de la concaténation des fichiers: {e}")

if __name__ == "__main__":
    clean_monthly_export_dir = os.path.join(outcomes, "clean_monthly_export")
    concatenated_output_file = os.path.join(outcomes, "latest_full_export.csv")
    archive_dir = os.path.join(outcomes, "archive")

    # Vérifier s'il existe déjà un fichier concaténés
    if os.path.exists(concatenated_output_file):
        archive_existing_file(concatenated_output_file, archive_dir)
    
    # Concaténer les fichiers
    concatenate_csv_files(clean_monthly_export_dir, concatenated_output_file)
