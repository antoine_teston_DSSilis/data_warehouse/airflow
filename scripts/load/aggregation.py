"""
# Traitements des raw data pour les patients hospitalisés
# Aggrégation des données pour accéder rapidement au fichier de concaténation
# permettant d'avoir les résumés de séjours des patients avec les durées de réveil, de séjour de réanimation
# et du nombre de thérapeutique administrés 
# Path: dags/rea_amiens/scripts/sejour.py
"""
import pandas as pd
import os
import logging
import sys
from datetime import datetime

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from extract import data_direction, datalake, outcomes
from custom_csv_logger import CSVLoggerHandler, CSVFormatter

# Charger la configuration du logging
logging.config.fileConfig('/opt/airflow/dags/rea_amiens/logging.conf')

# Créer un logger personnalisé
logger = logging.getLogger('custom')

def push_xcom(ti=None, **kwargs):
    # Utiliser le logger
    logger.info("Pushing XCom value")
    
    # Pusher un XCom
    if ti is not None:
        ti.xcom_push(key='my_key', value='my_value')
    else:
        logger.warning("No XCom pushed because 'ti' is None")

def pull_xcom(ti=None, **kwargs):
    # Utiliser le logger
    logger.info("Pulling XCom value")
    
    # Récupérer un XCom
    if ti is not None:
        xcom_value = ti.xcom_pull(key='my_key', task_ids='push_task')
        logger.info(f"Pulled XCom value: {xcom_value}")
    else:
        logger.warning("No XCom pulled because 'ti' is None")

def load_and_concat_files(directory):
    """
    Charge et concatène tous les fichiers CSV d'un répertoire en fonction de leur type (patient, sejour, medicament).

    Args:
        directory (str): Chemin du répertoire contenant les fichiers CSV.

    Returns:
        dict: Un dictionnaire contenant les DataFrames concaténés des patients, séjours et médicaments groupés par mois.
    """
    try:
        monthly_data = {}
        
        # Parcourir les fichiers du répertoire et les ajouter aux listes appropriées
        for file in os.listdir(directory):
            if file.endswith('.csv'):
                file_path = os.path.join(directory, file)
                df = pd.read_csv(file_path, sep=',')
                df['export_date'] = pd.to_datetime(df['export_date'], format='%y%m%d', errors='coerce')
                df['month_year'] = df['export_date'].dt.to_period('M')
                
                for period, group in df.groupby('month_year'):
                    month_year = period.strftime('%Y-%m')
                    
                    if month_year not in monthly_data:
                        monthly_data[month_year] = {'patient': [], 'sejour': [], 'medicament': []}
                    
                    if file.startswith('patient'):
                        monthly_data[month_year]['patient'].append(group)
                    elif file.startswith('sejour'):
                        monthly_data[month_year]['sejour'].append(group)
                    elif file.startswith('medicament'):
                        monthly_data[month_year]['medicament'].append(group)
        
        for month_year, data in monthly_data.items():
            monthly_data[month_year]['patient'] = pd.concat(data['patient'], ignore_index=True) if data['patient'] else pd.DataFrame()
            monthly_data[month_year]['sejour'] = pd.concat(data['sejour'], ignore_index=True) if data['sejour'] else pd.DataFrame()
            monthly_data[month_year]['medicament'] = pd.concat(data['medicament'], ignore_index=True) if data['medicament'] else pd.DataFrame()
        
        logger.info("Fichiers chargés et concaténés par mois.")
        return monthly_data
    except Exception as e:
        logger.error(f"Erreur lors du chargement des fichiers: {e}")
        raise

def format_timedelta(td):
    """
    Formate un objet timedelta en une chaîne de caractères au format 'Dd HH:MM:SS'.

    Args:
        td (Timedelta): Objet Timedelta.

    Returns:
        str: Chaîne de caractères formatée.
    """
    total_seconds = int(td.total_seconds())
    days, remainder = divmod(total_seconds, 86400)
    hours, remainder = divmod(remainder, 3600)
    minutes, seconds = divmod(remainder, 60)
    return f"{days}d {hours:02}:{minutes:02}:{seconds:02}" if days > 0 else f"{hours:02}:{minutes:02}:{seconds:02}"

def calculate_durations(sejour_df):
    """
    Calcule la durée entre les colonnes 'ENTREE_REVEIL', 'SORTIE_REVEIL', 'ENTREE_REA', 'SORTIE_REA' 
    et formate en un format lisible.

    Args:
        sejour_df (DataFrame): DataFrame pandas contenant les données des séjours.

    Returns:
        DataFrame: DataFrame avec les nouvelles colonnes de durée formatée.
    """
    try:
        sejour_df['ENTREE_REVEIL'] = pd.to_datetime(sejour_df['ENTREE_REVEIL'], format='%d/%m/%Y %H:%M:%S')
        sejour_df['SORTIE_REVEIL'] = pd.to_datetime(sejour_df['SORTIE_REVEIL'], format='%d/%m/%Y %H:%M:%S')
        sejour_df['ENTREE_REA'] = pd.to_datetime(sejour_df['ENTREE_REA'], format='%d/%m/%Y %H:%M:%S')
        sejour_df['SORTIE_REA'] = pd.to_datetime(sejour_df['SORTIE_REA'], format='%d/%m/%Y %H:%M:%S')
        
        sejour_df['ID_SEJOUR'] = sejour_df['ID_SEJOUR'].astype(str)
        sejour_df['ID_PATIENT'] = sejour_df['ID_PATIENT'].astype(str)

        sejour_df['duration_reveil'] = sejour_df['SORTIE_REVEIL'] - sejour_df['ENTREE_REVEIL']
        sejour_df['duration_reveil'] = sejour_df['duration_reveil'].fillna(pd.Timedelta(seconds=0))
        
        sejour_df['duration_rea'] = sejour_df['SORTIE_REA'] - sejour_df['ENTREE_REA']
        sejour_df['duration_rea'] = sejour_df['duration_rea'].fillna(pd.Timedelta(seconds=0))
        
        sejour_df['duration_reveil_formatted'] = sejour_df['duration_reveil'].apply(format_timedelta)
        sejour_df['duration_rea_formatted'] = sejour_df['duration_rea'].apply(format_timedelta)
        
        logger.info("Durées calculées et formatées.")
        return sejour_df
    except Exception as e:
        logger.error(f"Erreur lors du calcul des durées: {e}")
        raise

def patient_sex(patient_df):
    """
    Compte le nombre de patients par sexe.

    Args:
        patient_df (DataFrame): DataFrame pandas contenant les données des patients.

    Returns:
        DataFrame: DataFrame avec le nombre de patients par sexe.
    """
    try:
        patient_df['ID_PATIENT'] = patient_df['ID_PATIENT'].astype(str)
        patient_sexe = patient_df.groupby('ID_PATIENT')['SEXE'].first().reset_index()
        logger.info("Récupération des données patients")
        return patient_sexe
    except Exception as e:
        logger.error(f"Erreur dans la récupération des données patient: {e}")
        raise
    
def count_medicament_by_sejour(medicament_df):
    """
    Compte le nombre de lignes de médicaments par 'ID_SEJOUR'.

    Args:
        medicament_df (DataFrame): DataFrame pandas contenant les données de médicaments.

    Returns:
        DataFrame: DataFrame avec le nombre de médicaments par 'ID_SEJOUR'.
    """
    try:
        medicament_df['ID_SEJOUR'] = medicament_df['ID_SEJOUR'].astype(str)
        medicament_count = medicament_df.groupby('ID_SEJOUR').size().reset_index(name='medicament_count')
        logger.info("Nombre de médicaments par séjour calculé.")
        return medicament_count
    except Exception as e:
        logger.error(f"Erreur lors du comptage des médicaments par séjour: {e}")
        raise

def aggregate_data(df, patient_sexe_df, medicament_count_df):
    """
    Agrège les données pour obtenir la durée moyenne par 'ID_SEJOUR' et formate en un format lisible.

    Args:
        df (DataFrame): DataFrame pandas contenant les données avec les durées.
        medicament_count_df (DataFrame): DataFrame pandas contenant le nombre de médicaments par séjour.

    Returns:
        DataFrame: DataFrame agrégée avec la durée moyenne formatée en un format lisible.
    """
    try:
        df['duration_seconds_reveil'] = df['duration_reveil'].dt.total_seconds()
        df['duration_seconds_rea'] = df['duration_rea'].dt.total_seconds()
        
        aggregated_df = df.groupby('ID_SEJOUR').agg({
            'ID_PATIENT' : 'first',
            'duration_seconds_reveil': 'max',
            'duration_seconds_rea': 'max'
        }).reset_index()

        aggregated_df['average_duration_reveil_formatted'] = aggregated_df['duration_seconds_reveil'].apply(
            lambda x: format_timedelta(pd.Timedelta(seconds=x))
        )

        aggregated_df['average_duration_rea_formatted'] = aggregated_df['duration_seconds_rea'].apply(
            lambda x: format_timedelta(pd.Timedelta(seconds=x))
        )

        aggregated_df = aggregated_df.drop(columns=['duration_seconds_reveil', 'duration_seconds_rea'])

        # Merge with Patient sex 
        aggregated_df = aggregated_df.merge(patient_sexe_df, on='ID_PATIENT', how='left')

        # Merge with medicament count
        aggregated_df = aggregated_df.merge(medicament_count_df, on='ID_SEJOUR', how='left')
        aggregated_df['medicament_count'] = aggregated_df['medicament_count'].fillna(0).astype(int)

        logger.info("Données agrégées et durées formatées.")
        return aggregated_df
    except Exception as e:
        logger.error(f"Erreur lors de l'agrégation des données: {e}")
        raise

def save_to_csv(df, output_file):
    """
    Sauvegarde le DataFrame dans un fichier CSV.

    Args:
        df (DataFrame): DataFrame pandas à sauvegarder.
        output_file (str): Chemin du fichier de sortie.
    """
    try:
        os.makedirs(os.path.dirname(output_file), exist_ok=True)
        df.to_csv(output_file, index=False)
        logger.info(f"Fichier agrégé sauvegardé sous : {output_file}")
    except Exception as e:
        logger.error(f"Erreur lors de la sauvegarde du fichier: {e}")
        raise

def main(directory, monthly_output_dir):
    """
    Fonction principale pour charger, traiter, agréger et sauvegarder les données par mois.
    """
    start_time = datetime.now()
    logger.info(f"Start time: {start_time}")

    try:
        monthly_data = load_and_concat_files(directory)
        for month_year, data in monthly_data.items():
            sejour_df = calculate_durations(data['sejour'])
            patient_sexe_df = patient_sex(data['patient'])
            medicament_count_df = count_medicament_by_sejour(data['medicament'])
            aggregated_df = aggregate_data(sejour_df, patient_sexe_df, medicament_count_df)
            output_file = os.path.join(monthly_output_dir, f"aggregated_{month_year}.csv")
            save_to_csv(aggregated_df, output_file)
    except Exception as e:
        logger.error(f"Erreur dans le traitement des données : {e}")

    end_time = datetime.now()
    duration = end_time - start_time

    logger.info(f"End time: {end_time}")
    logger.info(f"Duration: {duration}")

if __name__ == "__main__":
    directory = datalake
    monthly_output_dir = os.path.join(outcomes, "clean_monthly_export")
    main(directory, monthly_output_dir)
    push_xcom()
    pull_xcom()
