import pandas as pd
import os

# Assurez-vous que les chemins d'accès nécessaires sont dans le PYTHONPATH
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from extract import datalake

def extract_drug_names(datalake_path, filename='medicament_data.csv'):
    """
    Extrait tous les noms de drogues ('NOM_DROGUE') du fichier médicament_data.csv situé dans le datalake.

    Args:
        datalake_path (str): Chemin du datalake où se trouve le fichier médicament_data.csv.
        filename (str): Nom du fichier médicament. Par défaut 'medicament_data.csv'.

    Returns:
        list: Liste des noms de drogues extraits.
    """
    file_path = os.path.join(datalake_path, filename)

    try:
        # Lire le fichier CSV
        df = pd.read_csv(file_path)
        
        # Vérifier si la colonne 'NOM_DROGUE' existe dans le DataFrame
        if 'NOM_DROGUE' in df.columns:
            drug_names = df['NOM_DROGUE'].dropna().unique().tolist()
            return drug_names
        else:
            print(f"La colonne 'NOM_DROGUE' n'existe pas dans le fichier {filename}.")
            return []

    except FileNotFoundError:
        print(f"Le fichier {filename} n'a pas été trouvé dans le chemin {datalake_path}.")
        return []
    except Exception as e:
        print(f"Une erreur s'est produite lors de la lecture du fichier {filename}: {e}")
        return []

if __name__ == "__main__":
    datalake_path = datalake  # Utiliser la variable 'datalake' importée de 'extract'
    drug_names = extract_drug_names(datalake_path)
    
    if drug_names:
        print(f"Nom des drogues extraites ({len(drug_names)}):")
        for name in drug_names:
            print(name)
    else:
        print("Aucun nom de drogue n'a été extrait.")
