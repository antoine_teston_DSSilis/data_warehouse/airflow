"""
    # Traitements des raw data pour les patients hospitalisés
    # Phase de Datamanagement avec vérification des distincts
    # Pour l'instant ne rien faire des doublons et les stocker dans un fichier csv
    # Future étape à prévoir d'affiner les critères de group by en fonction des spécificités des données 
    # genre clé_primaire/secondaire + date (ou pas si cas de MAJ au fil des exportations)
    # Path: dags/rea_amiens/scripts/sejour.py
"""

# Importation des libraries
import pandas as pd
import os
import sys
import logging
from datetime import datetime

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from extract import data_direction, datalake, outcomes
from custom_csv_logger import CSVLoggerHandler, CSVFormatter

# Charger la configuration du logging
logging.config.fileConfig('/opt/airflow/dags/rea_amiens/logging.conf')

# Créer un logger personnalisé
logger = logging.getLogger('custom')

def push_xcom(ti=None, **kwargs):
    # Utiliser le logger
    logger.info("Pushing XCom value")
    
    # Pusher un XCom
    if ti is not None:
        ti.xcom_push(key='my_key', value='my_value')
    else:
        logger.warning("No XCom pushed because 'ti' is None")

def pull_xcom(ti=None, **kwargs):
    # Utiliser le logger
    logger.info("Pulling XCom value")
    
    # Récupérer un XCom
    if ti is not None:
        xcom_value = ti.xcom_pull(key='my_key', task_ids='push_task')
        logger.info(f"Pulled XCom value: {xcom_value}")
    else:
        logger.warning("No XCom pulled because 'ti' is None")
    
def load_csv(file_path):
    """
    Charge le fichier CSV et retourne un DataFrame pandas.
    """
    try:
        df = pd.read_csv(file_path)
        return df
    except FileNotFoundError:
        logger.error(f"File not found: {file_path}")
        return None
    except Exception as e:
        logger.error(f"Error loading CSV: {e}")
        return None

def find_duplicates(df, exclude_column):
    """
    Trouve les doublons dans le DataFrame en excluant une colonne spécifique.
    
    Arguments:
    df -- DataFrame pandas contenant les données du CSV
    exclude_column -- Nom de la colonne à exclure de la vérification des doublons
    """
    if exclude_column not in df.columns:
        logger.error(f"La colonne {exclude_column} n'existe pas dans le DataFrame.")
        return None

    columns_to_check = [col for col in df.columns if col != exclude_column]
    
    # Vérifie les doublons en excluant la colonne spécifiée
    duplicate_rows = df[df.duplicated(subset=columns_to_check, keep=False)]
    
    return duplicate_rows

def export_duplicates(duplicates, output_path):
    """
    Exporte les lignes en double dans un fichier CSV.
    
    Arguments:
    duplicates -- DataFrame contenant les lignes dupliquées
    output_path -- Chemin du fichier de sortie
    """
    try:
        duplicates.to_csv(output_path, index=False)
        logger.info(f"Doublons exportés vers {output_path}")
    except Exception as e:
        logger.error(f"Erreur lors de l'exportation des doublons: {e}")
        
def check_duplicates_in_directory(directory_path, exclude_column, output_directory):
    """
    Vérifie les doublons dans tous les fichiers CSV d'un répertoire en excluant une colonne spécifique
    et exporte les doublons dans un fichier CSV distinct dans un dossier spécifié.
    
    Arguments:
    directory_path -- Chemin du répertoire contenant les fichiers CSV
    exclude_column -- Nom de la colonne à exclure de la vérification des doublons
    output_directory -- Chemin du répertoire de sortie pour les fichiers de doublons
    """
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)
    
    start_time = datetime.now()
    logger.info(f"Start time: {start_time}")

    total_lines = 0
    modality_count = {}
    
    for file_name in os.listdir(directory_path):
        if file_name.endswith(".csv"):
            file_path = os.path.join(directory_path, file_name)
            logger.info(f"Vérification des doublons dans le fichier: {file_name}")
            df = load_csv(file_path)
            if df is not None:
                total_lines += len(df)
                
                for column in df.columns:
                    modality_count[column] = df[column].nunique()
                    
                duplicates = find_duplicates(df, exclude_column)
                if duplicates is not None and not duplicates.empty:
                    output_file_name = file_name.replace(".csv", "_double.csv")
                    output_path = os.path.join(output_directory, output_file_name)
                    export_duplicates(duplicates, output_path)
                else:
                    logger.info(f"Aucun doublon trouvé dans {file_name} (en excluant la colonne '{exclude_column}').")
    
    end_time = datetime.now()
    duration = end_time - start_time
    
    logger.info(f"End time: {end_time}")
    logger.info(f"Duration: {duration}")
    logger.info(f"Total lines processed: {total_lines}")
    logger.info(f"Modality count: {modality_count}")
                    
if __name__ == "__main__":
    directory_path = datalake
    exclude_column = "export_date"
    output_directory = os.path.join(outcomes, "dataquality")
    check_duplicates_in_directory(directory_path, exclude_column, output_directory)
    push_xcom()
    pull_xcom()
