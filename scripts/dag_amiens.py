from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from datetime import datetime, timedelta
import logging

with DAG(
    'Anesth_sejour',
    description='Séjour des patients hospitalisés',
    schedule=timedelta(days=30),
    start_date=datetime(2024, 3, 1),
    catchup=False,
) as dag:

    t1 = BashOperator(
        task_id='listing_direction',
        bash_command='ls /opt/airflow/dags/rea_amiens/scripts/extract'
    )
    
    t3 = BashOperator(
        task_id='Initial_extraction',
        bash_command='python3 /opt/airflow/dags/rea_amiens/scripts/extract/initial_extraction.py'
    )
    
    t4 = BashOperator(
        task_id='Upload_Datalake',
        bash_command='python3 /opt/airflow/dags/rea_amiens/scripts/extract/upload_data.py'
    )
    
    t5 = BashOperator(
        task_id='Check_distinct',
        bash_command='python3 /opt/airflow/dags/rea_amiens/scripts/transform/distinct_management.py'
    )
    
    t6 = BashOperator(
        task_id='Dictionnary',
        bash_command='python3 /opt/airflow/dags/rea_amiens/scripts/transform/dictionnary.py'
    )
    
    t7 = BashOperator(
        task_id='Aggregation',
        bash_command='python3 /opt/airflow/dags/rea_amiens/scripts/load/aggregation.py'
    )
    
    t8 = BashOperator(
        task_id='Concatenation',
        bash_command='python3 /opt/airflow/dags/rea_amiens/scripts/load/concatenation.py'
    )
    
    t1 >> t3 >> t4 >> t5 >> t6 >> t7 >> t8
