import logging
import csv
import os

class CSVLoggerHandler(logging.Handler):
    def __init__(self, filename, mode='a'):
        super().__init__()
        self.filename = filename
        self.mode = mode

        # Créer le répertoire si nécessaire
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        # Écrire les en-têtes si le fichier est vide
        if not os.path.exists(self.filename) or os.path.getsize(self.filename) == 0:
            with open(self.filename, 'w', newline='') as file:
                writer = csv.writer(file)
                writer.writerow(['asctime', 'name', 'levelname', 'message'])

    def emit(self, record):
        log_entry = self.format(record).split(',')
        with open(self.filename, self.mode, newline='') as file:
            writer = csv.writer(file)
            writer.writerow(log_entry)

# Formatter personnalisé pour le CSV
class CSVFormatter(logging.Formatter):
    def format(self, record):
        return f'{self.formatTime(record)},{record.name},{record.levelname},{record.getMessage()}'

# Utilisation des classes de logging personnalisées
if __name__ == "__main__":
    log_file_path = '/opt/airflow/dags/rea_amiens/outcomes/logs/task_logs.csv'

    # Configurer le logger
    logger = logging.getLogger('custom_csv_logger')
    logger.setLevel(logging.INFO)

    # Configurer le gestionnaire de logs CSV
    csv_handler = CSVLoggerHandler(log_file_path)
    csv_handler.setFormatter(CSVFormatter())
    logger.addHandler(csv_handler)
