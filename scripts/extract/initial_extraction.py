# Traitements des raw data pour les patients hospitalisés
# Path: dags/rea_amiens/scripts/sejour.py

# Importation des libraries
from datetime import datetime
import sys
import logging
import logging.config
import os

# Ajouter le répertoire racine au PYTHONPATH
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..')))
from custom_csv_logger import CSVLoggerHandler, CSVFormatter

# Charger la configuration du logging
logging.config.fileConfig('/opt/airflow/dags/rea_amiens/logging.conf')
# Logger personnalisé
logger = logging.getLogger('custom')

# Chemin absolu du fichier du script courant
direction = os.path.dirname(os.path.abspath(__file__))

# Chemin du répertoire où les fichiers doivent être présents
data_direction = os.path.join(direction, '..', '..', 'data')
data_direction = os.path.normpath(data_direction)
outcomes = os.path.join(direction, '..', '..', 'outcomes')
outcomes = os.path.normpath(outcomes)
datalake = os.path.join(direction, '..', '..', 'outcomes', 'datalake')
datalake = os.path.normpath(datalake)
    
def push_xcom(ti=None, **kwargs):
    # Utiliser le logger
    logger.info("Pushing XCom value")
    
    # Pusher un XCom
    if ti is not None:
        ti.xcom_push(key='my_key', value='my_value')
    else:
        logger.warning("No XCom pushed because 'ti' is None")

def pull_xcom(ti=None, **kwargs):
    # Utiliser le logger
    logger.info("Pulling XCom value")
    
    # Récupérer un XCom
    if ti is not None:
        xcom_value = ti.xcom_pull(key='my_key', task_ids='Init')
        logger.info(f"Pulled XCom value: {xcom_value}")
    else:
        logger.warning("No XCom pulled because 'ti' is None")

def main():
    # Format de la date dans les noms de fichiers
    date_format = "%y%m%d"  # Format de date comme '230101' 

    # Lire les dates depuis les noms des fichiers dans datalake
    latest_dates = {}
    for file in os.listdir(datalake):
        if file.startswith("data_") and file.endswith(".csv"):
            parts = file.split('_')
            if len(parts) > 2:  # Assurer que le nom contient assez de parties pour inclure une date
                date_str = parts[2].split('.')[0]
                try:
                    current_date = datetime.strptime(date_str, date_format)
                    file_type = parts[1]
                    if file_type not in latest_dates or current_date > latest_dates[file_type]:
                        latest_dates[file_type] = current_date
                except ValueError:
                    logger.error(f"Erreur de format de date pour le fichier {file}: {date_str} ne correspond pas au format attendu {date_format}.")

    # Vérifier s'il existe des dossiers plus anciens dans le répertoire source
    older_exists = False
    for item in os.listdir(data_direction):
        item_path = os.path.join(data_direction, item)
        if os.path.isdir(item_path):
            try:
                # Vérifier si le nom du dossier contient bien 6 chiffres à la fin (format de date)
                date_str = item[-6:]
                if date_str.isdigit():
                    item_date = datetime.strptime(date_str, date_format)
                    for file_type, latest_date in latest_dates.items():
                        if item_date < latest_date:
                            logger.info(f"Trouvé un dossier plus ancien pour {file_type}: {item}")
                            older_exists = True
                else:
                    logger.warning(f"Nom de dossier sans date valide: {item}")
            except ValueError:
                logger.error(f"Erreur de format de date pour le dossier {item}: {date_str} ne correspond pas au format attendu {date_format}.")

    # Résumer si des dossiers plus anciens ont été trouvés
    if older_exists:
        logger.info("Des dossiers plus anciens ont été trouvés.")
    else:
        logger.info("Aucun dossier plus ancien n'a été trouvé.")

if __name__ == "__main__":
    main()
    push_xcom()
