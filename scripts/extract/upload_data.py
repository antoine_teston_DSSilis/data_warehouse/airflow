"""
    # Traitements des raw data pour les patients hospitalisés
    # Path: dags/rea_amiens/scripts/sejour.py
"""

# Importation des libraries
import pandas as pd
import os 
from datetime import datetime
import sys
import logging
import logging.config

# Ajouter le répertoire racine au PYTHONPATH
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from extract import data_direction, datalake

# Charger la configuration du logging
logging.config.fileConfig('/opt/airflow/dags/rea_amiens/logging.conf')

# Créer un logger personnalisé
logger = logging.getLogger('custom')

def push_xcom(ti=None, **kwargs):
    # Utiliser le logger
    logger.info("Pushing XCom value")
    
    # Pusher un XCom
    if ti is not None:
        ti.xcom_push(key='my_key', value='my_value')
    else:
        logger.warning("No XCom pushed because 'ti' is None")

def pull_xcom(ti=None, **kwargs):
    # Utiliser le logger
    logger.info("Pulling XCom value")
    
    # Récupérer un XCom
    if ti is not None:
        xcom_value = ti.xcom_pull(key='my_key', task_ids='push_task')
        logger.info(f"Pulled XCom value: {xcom_value}")
    else:
        logger.warning("No XCom pulled because 'ti' is None")
    
# Format de la date utilisée dans les noms de fichiers
date_format = "%y%m%d"  # Format de date comme '230101' pour le 1er janvier 2023

def get_latest_date_from_export_date():
    """Extrait la date la plus récente de la colonne 'export_date' dans tous les fichiers CSV du datalake."""
    latest_date = None
    for file in os.listdir(datalake):
        if file.endswith('.csv'):
            file_path = os.path.join(datalake, file)
            try:
                df = pd.read_csv(file_path)
                if 'export_date' in df.columns:
                    df['export_date'] = pd.to_datetime(df['export_date'], format=date_format, errors='coerce')
                    max_date = df['export_date'].max()
                    if max_date and (not latest_date or max_date > latest_date):
                        latest_date = max_date
            except pd.errors.EmptyDataError:
                logger.error(f"Le fichier {file} est vide.")
            except Exception as e:
                logger.error(f"Erreur lors de la lecture du fichier {file}: {e}")
    return latest_date

def get_sorted_folders(directory):
    """Retourne une liste de dossiers triés par date extraite du nom du dossier."""
    folders = []
    for item in os.listdir(directory):
        item_path = os.path.join(directory, item)
        if os.path.isdir(item_path):
            try:
                folder_date = datetime.strptime(item.split('_')[-1], date_format)
                folders.append((folder_date, item))
            except ValueError:
                continue
    folders.sort()
    return [folder[1] for folder in folders]

def load_files_with_export_date(directory, folder_name):
    """Charge les fichiers d'un dossier et ajoute une colonne 'export_date'."""
    folder_path = os.path.join(directory, folder_name)
    files_data = {}
    for file in os.listdir(folder_path):
        if file.endswith('.csv'):
            file_type = file.split('_')[0]
            file_path = os.path.join(folder_path, file)
            df = pd.read_csv(file_path, sep=';')
            df['export_date'] = folder_name.split('_')[-1]
            if file_type in files_data:
                files_data[file_type] = pd.concat([files_data[file_type], df], ignore_index=True)
            else:
                files_data[file_type] = df
    return files_data

def update_data_files(data_frames, datalake):
    """Mise à jour des fichiers dans le datalake avec les nouvelles données fusionnées."""
    for file_type, new_df in data_frames.items():
        file_path = os.path.join(datalake, f"{file_type}_data.csv")
        if os.path.exists(file_path):
            existing_df = pd.read_csv(file_path)
            new_df = pd.concat([existing_df, new_df], ignore_index=True)
        new_df.to_csv(file_path, index=False)
        logger.info(f"DataFrame pour {file_type} mis à jour et sauvegardé sous : {file_path}")

def main():
    # Application des fonctions
    latest_date = get_latest_date_from_export_date()
    if not latest_date:
        latest_date = datetime.min

    sorted_folders = get_sorted_folders(data_direction)

    for folder in sorted_folders:
        folder_date = datetime.strptime(folder.split('_')[-1], date_format)
        if folder_date > latest_date:
            data_frames = load_files_with_export_date(data_direction, folder)
            update_data_files(data_frames, datalake)
        else:
            logger.info(f"Le dossier {folder} a déjà été traité.")

    logger.info("Traitement des dossiers terminé.")

if __name__ == "__main__":
    main()
    push_xcom()
